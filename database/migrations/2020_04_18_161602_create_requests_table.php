<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('priority');
            $table->string('message')->nullable();
            $table->string('status_request');
            $table->string('status_return');
            $table->date('date_approved')->nullable();
            $table->date('date_returned')->nullable();
            $table->date('viewed_decline')->nullable();
            $table->date('declined_date')->nullable();

            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('asset_id')->references('id')->on('assets');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
