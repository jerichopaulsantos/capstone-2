<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\Asset;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $assets = DB::table('assets')
      ->join('categories', 'assets.category_id', '=', 'categories.id')
      ->select('assets.*', 'categories.name as categoryname', DB::raw(" (SELECT users.name from requests INNER JOIN users ON requests.user_id=users.id WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `username` "))
      ->paginate(4);

        return view('admin.assets.assets')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.assets.create-asset')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'serial_number' => 'required|unique:assets,serial_number'
            ]);

        $featured = $request->img_path;

        //Make a new image file name
        $featured_new_name = time().$featured->getClientOriginalName();

        //Move to the uploads folder
        $featured->move('uploads/', $featured_new_name);


        $asset = new Asset;

        $asset->name = $request->name;
        $asset->processor = $request->processor;
        $asset->storage = $request->storage;
        $asset->memory = $request->memory;
        $asset->serial_number = $request->serial_number;
        $asset->accessories = $request->accessories;
        $asset->category_id = $request->category_id;
        $asset->img_path = 'uploads/'. $featured_new_name;

        $asset->save();

        return redirect()->route('assets.assets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::find($id);
        $categories = Category::all();
        return view('admin.assets.edit-asset')->with('asset', $asset)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $asset = Asset::find($id);

        $this->validate($request, [
            'serial_number' => 'required|unique:assets,serial_number,'.$asset->id.','
            ]);

        if ($request->img_path != null) {
            $featured = $request->img_path;

            //Make a new image file name
            $featured_new_name = time().$featured->getClientOriginalName();

            //Move to the uploads folder
            $featured->move('uploads/', $featured_new_name);

            $asset->img_path = 'uploads/'. $featured_new_name;

        }

        $asset->name = $request->name;
        $asset->processor = $request->processor;
        $asset->storage = $request->storage;
        $asset->memory = $request->memory;
        $asset->serial_number = $request->serial_number;
        $asset->accessories = $request->accessories;
        $asset->category_id = $request->category_id;

        $asset->save();

        return redirect()->route('assets.assets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::find($id);
        $asset->delete();

        return redirect('/assets/assets');
    }
}