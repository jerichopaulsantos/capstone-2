<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('owner-middleware')->only(['admin', 'createadmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users = User::where([ 'is_admin'=>false, 'is_owner'=>false ])->get();

       return view('admin.users.users', ['users'=>$users, 'isadmin'=>false]);
    }

    public function admin()
    {
       $users = User::where([ 'is_admin'=>true, 'is_owner'=>false ])->get();

       return view('admin.users.users', ['users'=>$users, 'isadmin'=>true]);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.users.create-user', ['isadmin'=>false]);
    }
    public function createadmin()
    {
       return view('admin.users.create-user', ['isadmin'=>true]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $this->validate($request, [
        //     'password' => 'required|confirmed'
        //     ]);


        $validator = Validator::make($request->all(), [
                'contact_number' => 'required|unique:users,contact_number',
                'employee_number' => 'required|unique:users,employee_number',
                'email' => 'required|unique:users,email|email',
                'password' => 'required|confirmed|min:8'
            ]);

        if ($validator->fails()){
            return redirect('/users/create-user')->withInput()->with('errors', $validator->errors());
        }

        $featured = $request->img_path;

        //Make a new image file name
        $featured_new_name = time().$featured->getClientOriginalName();

        //Move to the uploads folder
        $featured->move('uploads/', $featured_new_name);


        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->position = $request->position;
        $user->employee_number = $request->employee_number;
        $user->contact_number = $request->contact_number;
        $user->is_admin = false;
        $user->is_owner = false;
        $user->password = Hash::make($request->password);
        $user->img_path = 'uploads/'. $featured_new_name;

        $user->save();

        // dd($request->all());

        return redirect()->route('users.users');
  
    }

    public function adminstore(Request $request)
    {

        $validator = Validator::make($request->all(), [
                'contact_number' => 'required|unique:users,contact_number',
                'employee_number' => 'required|unique:users,employee_number',
                'email' => 'required|unique:users,email|email',
                'password' => 'required|confirmed|min:8'
            ]);

        if ($validator->fails()){
            return redirect('/users/create-admin')->withInput()->with('errors', $validator->errors());
        }

        $featured = $request->img_path;

        //Make a new image file name
        $featured_new_name = time().$featured->getClientOriginalName();

        //Move to the uploads folder
        $featured->move('uploads/', $featured_new_name);


        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->position = $request->position;
        $user->employee_number = $request->employee_number;
        $user->contact_number = $request->contact_number;
        $user->is_admin = true;
        $user->is_owner = false;
        $user->password = Hash::make($request->password);
        $user->img_path = 'uploads/'. $featured_new_name;

        $user->save();

        // dd($request->all());

        return redirect()->route('users.admin');
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $user = User::find($id);
        $issued_requests = \App\Request::where('user_id', Auth::user()->id)
                                    ->where('status_request', 'approved' )
                                    ->get();
        $declined_requests = \App\Request::where('user_id', Auth::user()->id)
                                    ->where('status_request', 'declined' )
                                    ->get();
        return view('admin.users.view-user')->with('user', $user)->with('issued_requests', $issued_requests)->with('declined_requests', $declined_requests);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $profile = $request->query('profile');

        $user = User::find($id);
        return view('admin.users.edit-user')->with('user', $user)->with('profile', $profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       

        $user = User::find($id);

        $validations = [
                'contact_number' => 'required|unique:users,contact_number,'.$id,
                'employee_number' => 'required|unique:users,employee_number,'.$id,
                'email' => 'required|unique:users,email,'. $id .'|email'
            ];

        if (!empty($request->password)){
           $validations['password'] = 'required|confirmed|min:8'; 
        }

        $validator = Validator::make($request->all(), $validations);

        if ($validator->fails()){
            return redirect('/users/edit-user/'.$id)->with('errors', $validator->errors())
                                                    ->with('user', $user);
        }

        if ($request->img_path != null) {
            $featured = $request->img_path;

            //Make a new image file name
            $featured_new_name = time().$featured->getClientOriginalName();

            //Move to the uploads folder
            $featured->move('uploads/', $featured_new_name);

            $user->img_path = 'uploads/'. $featured_new_name;

        }

        if (!empty($request->password)){
            $user->password = Hash::make($request->password); 
        }


        $user->name = $request->name;
        $user->email = $request->email;
        $user->position = $request->position;
        $user->employee_number = $request->employee_number;
        $user->contact_number = $request->contact_number;
        // $user->password = Hash::make($request->password);

        $user->save();

        if ($request->query('profile')=='true'){
            return redirect('/users/view-user/' . $id);
        }

        if ($user->is_admin){
           return redirect('/users/admin'); 
        }
        return redirect('/users/users');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        if ($user->is_admin){
           return redirect('/users/admin'); 
        }
        return redirect('/users/users');
    }
}
