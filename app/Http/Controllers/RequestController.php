<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;
use Auth;
use Session;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $allRequests = [];

      $requests = \App\Request::where('status_request', 'pending')->get();
      foreach ($requests as $request) {
            $request->type='Request';
            array_push($allRequests, $request);
        }  
      $returns = \App\Request::where('status_return', 'pending')->get();  
      foreach ($returns as $return) {
            $return->type='Return';
            array_push($allRequests, $return);
        }  
      
      $laptopId = Category::where('name', 'laptop')->first()->id;
      $tabletId = Category::where('name', 'tablet')->first()->id;

      $laptop_issued = DB::table('assets')
      ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
      ->where('category_id', $laptopId)
      ->having('count', 0)
      ->get();

      $tablet_issued = DB::table('assets')
      ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
      ->where('category_id', $tabletId)
      ->having('count', 0)
      ->get();

      $laptop_count = count($laptop_issued);
      $tablet_count = count($tablet_issued);


      return view('admin.requests.requests')->with('requests', $allRequests)
                                            ->with('laptop_count', $laptop_count)
                                            ->with('tablet_count', $tablet_count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAsset = new \App\Request;

        $requestAsset->priority = $request->priority;
        $requestAsset->message = $request->message;
        $requestAsset->status_request = 'pending';
        $requestAsset->status_return = 'unreturned';
        $requestAsset->asset_id = $request->asset_id;
        $requestAsset->user_id = Auth::user()->id;

        $requestAsset->save();

        return view('user.request-confirm');
    }

    public function approveRequest(Request $rq)
    {
        $request = \App\Request::find($rq->approveRequestId);

        $issued_count = \App\Request::where('asset_id', $request->asset_id)
                                      ->where('status_request', 'approved')
                                      ->where('status_return', '<>', 'approved')
                                      ->count();
        if ($issued_count > 0) {
            Session::flash('error', 'That asset has been issued to an employee');
        }
        else {
            $request->status_request = 'approved';
            $request->date_approved = date('Y/m/d H:i:s');

            $request->save();    
        }
        
        

        return redirect()->route('requests.requests');
    }


    public function approveReturn(Request $rq)
    {
        $request = \App\Request::find($rq->approveReturnId);
        $request->status_return = 'approved';
        $request->date_returned = date('Y/m/d H:i:s');

        $request->save();

        return redirect()->route('requests.requests');
    }


   public function declineRequest(Request $rq)
    {
        $request = \App\Request::find($rq->declineRequestId);
        $request->status_request = 'declined';
        $request->declined_date = date('Y/m/d H:i:s');

        $request->save();

        return redirect()->route('requests.requests');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
