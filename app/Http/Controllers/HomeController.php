<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\Category;
use App\User;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function homeresolver()
    {
       if(Auth::user()->is_admin){
            return redirect()->route('home');
        }
        else {
            return redirect()->route('users.index');                
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      $requestCount = \App\Request::where('status_request', 'pending')->count();
      $returnCount = \App\Request::where('status_return', 'pending')->count();  

      $allRequestCount = $requestCount + $returnCount;

      $laptopId = Category::where('name', 'laptop')->first()->id;
      $tabletId = Category::where('name', 'tablet')->first()->id;

      $laptop_issued = DB::table('assets')
      ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
      ->where('category_id', $laptopId)
      ->having('count', 0)
      ->get();

      $tablet_issued = DB::table('assets')
      ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
      ->where('category_id', $tabletId)
      ->having('count', 0)
      ->get();

      $laptop_count = count($laptop_issued);
      $tablet_count = count($tablet_issued);


      $user_count = User::where([ 'is_admin'=>false, 'is_owner'=>false ])->count();


      return view('home')->with('laptop_count', $laptop_count)
                         ->with('tablet_count', $tablet_count)
                         ->with('user_count', $user_count)
                         ->with('allRequestCount', $allRequestCount);

    }
    public function user()
    {
        $request = \App\Request::where('user_id', Auth::user()->id)
                                ->where(function ($q) {
                                    $q->where('status_request', 'pending')
                                    ->orWhere(function ($q) {
                                        $q->where('status_request',  'declined')
                                        ->whereNull('viewed_decline');
                                    })

                                    ->orWhere(function ($q) {
                                        $q->where('status_request',  'approved')
                                        ->where('status_return', '<>', 'approved');
                                    });
                                })        


                                ->first();
        if ($request!=null && 
            $request->status_request=='declined'){
            $request->viewed_decline = date('Y/m/d H:i:s');

            $request->save();
        }

        $assetCount = Asset::count();
        return view('user.index')->with('request', $request)->with('assetCount', $assetCount);
    }
    public function asset()
    {
        $request = \App\Request::where('user_id', Auth::user()->id)
                                ->where('status_request',  'approved')
                                ->where('status_return', '<>', 'approved')
                                ->first();

        return view('user.asset')->with('request', $request );

        //dd($asset->asset);
    }
    public function request()
    {

        $assets = [];
        $laptopId = Category::where('name', 'laptop')->first()->id;
        $tabletId = Category::where('name', 'tablet')->first()->id;


        $laptop_issued  = DB::table('assets')
                        ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
                        ->where('category_id', $laptopId)
                        ->having('count', 0)
                        ->get();
       
        foreach ($laptop_issued as $laptop) {
              array_push($assets, $laptop);
          }  

        $tablet_issued = DB::table('assets')
                      ->select('assets.*', DB::raw(" (SELECT count(*) from requests WHERE asset_id=assets.id and status_request='approved' and status_return!='approved')as `count` "))
                      ->where('category_id', $tabletId)
                      ->having('count', 0)
                      ->get();

        foreach ($tablet_issued as $tablet) {
              array_push($assets, $tablet);
          } 


        $hasAsset = \App\Request::where('user_id', Auth::user()->id)
                                 ->where(function ($q) {
                                    $q->where('status_request', 'pending')                                   
                                     ->orWhere(function ($q) {
                                        $q->where('status_request',  'approved')
                                        ->where('status_return', '<>', 'approved');
                                    });
                                })
                                ->exists();

        return view('user.request')->with('assets', $assets)->with('hasAsset', $hasAsset);

    }
    // public function confirm()
    // {
    //     return view('user.request-confirm');
    // }

    // public function returnConfirm()
    // {
    //     return view('user.return-confirm');
    // }

    public function return($id)
    {
        $request = \App\Request::find($id);
        $request->status_return='pending';

        $request->save();

        return view('user.return-confirm');
    }
}
