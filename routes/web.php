<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home-resolver', 'HomeController@homeresolver');
Route::get('/users/view-user/{id}', 'UserController@view')->name('users.view');


Route::group(['middleware'=>['user-middleware']],function(){
	Route::get('/user/', 'HomeController@user')->name('users.index');
	Route::get('/user/asset', 'HomeController@asset');
	Route::get('/user/request', 'HomeController@request');
	Route::post('/user/request-return/{id}', 'HomeController@return');
	Route::post('/requests/store', 'RequestController@store');
//	Route::get('/user/request-confirm', 'HomeController@confirm')->name('users.request-confirm');
//	Route::get('/user/return-confirm', 'HomeController@returnConfirm');


});


Route::group(['middleware'=>['admin-middleware']],function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/assets/assets', 'AssetController@index')->name('assets.assets');
	Route::get('/assets/create-asset', 'AssetController@create');
	Route::get('/assets/edit-asset/{id}', 'AssetController@edit')->name('assets.edit');
	Route::post('/assets/store', 'AssetController@store');
	Route::post('/assets/update/{id}', 'AssetController@update')->name('assets.update');
	Route::get('/assets/delete/{id}', 'AssetController@destroy')->name('assets.delete');
	Route::get('/users/users', 'UserController@index')->name('users.users');
	Route::get('/users/edit-user/{id}', 'UserController@edit')->name('users.edit');
	Route::post('/users/update/{id}', 'UserController@update')->name('users.update');
	Route::get('/users/create-user', 'UserController@create');
	Route::get('/users/create-admin', 'UserController@createadmin');
	Route::get('/users/admin', 'UserController@admin')->name('users.admin');
	Route::get('/users/delete/{id}', 'UserController@destroy')->name('users.delete');
	Route::post('/users/store', 'UserController@store');
	Route::post('/users/admin-store', 'UserController@adminstore');

	Route::get('/requests/requests', 'RequestController@index')->name('requests.requests');
	Route::post('/requests/approve-request', 'RequestController@approveRequest');
	Route::post('/requests/decline-request', 'RequestController@declineRequest');	
	Route::post('/requests/approve-return', 'RequestController@approveReturn');

	Route::get('/user/request-confirm', 'HomeController@confirm')->name('users.request-confirm');
//	Route::get('/user/return-confirm', 'HomeController@returnConfirm');


});

