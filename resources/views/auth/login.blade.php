@extends('layouts.app')

@section('content')
<div class="container">

    <div class="col col-lg-5 col-md-7" style="background: #fff; min-height: 100vh;" >
        <div class="row justify-content-center" style="margin-top:80px">
            <div class="col-md-7">
                    <!-- <div>{{ __('Login') }}</div> -->
                    <div style="margin: 30px 0 50px 20px;"><img style="width: 100%;" src="{{ asset('images/skybrokers-app2.png') }}" alt="SkyBrokers"></div>

                    <div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                            <!-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>-->
                                <div class="col-md-12">
                                    <input style="border: none; border-bottom: 1px solid #ccc;"  placeholder="e-mail" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                            <!-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>-->
                                <div class="col-md-12">
                                    <input style="border: none; border-bottom: 1px solid #ccc;" placeholder="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row"  style="margin-top: 30px; margin-bottom: 30px;">
                                <div class="col">
                                    <div class="form-check">
                                        <input style="width: 4%;" class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label style="margin-top:12px;" class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>

                                        @if (Route::has('password.request'))
                                            <a style="float: right; font-size: 12px; margin-top:8px;margin-right:0px;padding-right:0px;" class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Password?') }}
                                            </a>
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary btpr">
                                        {{ __('Login') }}
                                    </button>


                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div> 
    </div>

    <div class="col col-lg-5 col-md-7">
        <img src="{{ asset('images/growth-2.png') }}" style="width:130%; margin: 60px 0 0 30px;">
    </div>

</div>
@endsection
