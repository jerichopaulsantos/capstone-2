@extends('layouts.app')


@section('content')

<div class="container" style="background-color: #fff;">

		<div class="col col-lg-10  justify-content-center">
			<img src="{{ asset('images/error-403.png') }}" style="width:50%; margin: 60px 0 0 350px;">
			<p style="font-size:15px;margin: 10px 0 0 230px; text-align:center;"><span style="color: #fff; font-size:30px;">You can't go here!</span><br>You will perish here. Please return to your <a href="/home-resolver" style="color:#ec6b65;">dashboard.</a></p>              
		</div>


</div>

@endsection