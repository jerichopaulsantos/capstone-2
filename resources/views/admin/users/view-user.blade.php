@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

<div class="container body">

    <div class="main-container">
        @if(Auth::user()->is_admin)
        <div class="col-md-3 left_col" style="height:1300px;">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
					  <li><a href="/home"><i class="fa fa-home mr10"></i> Dashboard</a>
					  </li>
					  <li><a href="/assets/assets"><i class="fa fa-desktop mr10"></i> Assets</a>
					  </li>
					  <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
					  </li>
					  <li><a href="/requests/requests"><i class="fa fa-edit mr10"></i> Requests</a>
					  </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                 
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 
        @else
          <div class="col-md-3 left_col" style="height:1300px;">
              <div class="left_col scroll-view">
                  <div class="clearfix"></div>
                  <br />
                  <!-- sidebar menu -->
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                      <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/user/"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="#"><i class="fa fa-desktop mr10"></i> Asset Issued</a>
                      </li>
                      <li><a href="/user/request"><i class="fa fa-desktop mr10"></i> Request Asset</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                    </div>
                  </div>
                  <!-- /sidebar menu -->
              </div>
          </div>       
        @endif

        <div class="top_nav">
        </div>

        <div class="row justify-content-center">
          <div class="col-md-10 mt-5">
              <div class="col-md-10">
                @if($user->is_admin)
                <p><a href="/users/admin"><i class="fa fa-angle-left no-mobile"></i> &nbsp; Back to Admin</a></p>
                @else
                
                @endif   
              </div>
              <div class="col-md-10">
                @if($user->is_admin)
                <h1>View Admin</h1>
                @else
                <h1>View Profile</h1>                
                @endif
              </div>
              <div class="col-md-2 mt-3">
              </div>
          </div>
        </div>


          <div class="row justify-content-center mt-5 mb-5">
            <div class="top_tiles col-md-10">

              <div class="col offset-lg-1 col-lg-10 col-md-10 widget_tally_box justify-content-center ">
                  <div class="x_panel ui-ribbon-container">
                    <div class="row">
                      <div class="col col-lg-4 col-md-4">
                        <div class="x_content">
                          <div class="flex">
                            <img src="/{{ $user->img_path }}" alt="..." class="img-circle profile_img">
                          </div>
                        </div>
                      </div>
                      <div class="col col-lg-8 col-md-8">
                          <div class="x_content">
                            <div class="card-title ml-2 pt-3" style="font-size:22px;font-weight: bold;"><a href="/users/view-user">{{ $user->name }}</a></div>
                            <p class="ml-2" style="margin-top: -10px; text-align:left;">{{ $user->position }}</p>
                            <div class="divider"></div>
                            <p class="text-left ml-2" style="font-size:16px;">
                              <small>Employee No</small><br><span style="font-size:16px;font-weight: bold;">{{ $user->employee_number }}</span><br>
                              <small>Email Address</small><br><span style="font-size:16px;font-weight: bold;">{{ $user->email }}</span><br>
                              <small>Contact Number</small><br><span style="font-size:16px;font-weight: bold;">{{ $user->contact_number }}</span><br>
                            </p>
                            @if(Auth::user()->is_admin)
                            <a href="{{route('users.edit', ['id' => $user->id])}}?profile=true" class="btn btn-primary btpr" >Edit</a>
                            <a href="{{route('users.delete', ['id' => $user->id])}}" class="btn btn-danger btda">Delete</a><br>
                            @else                             
                            <a class="btn btn-primary btpr" href="mailto:hr@skybrokers.com?Subject=REQUEST:%20Profile%20Edit">Request edit</a>
                            @endif
                          </div>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="x_content">
                        <h2 class="card-title ml-4" style="text-align:left;">Asset History</h2>

                        <div class="col">
                          <div class="x_panel">
                            <div class="x_title"  style="font-weight:bold;">Assets Issued</div>
                            <div class="x_content">
                              <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Device</th>
                                    <th>Date Approved</th>
                                    <th>Date Returned</th>
                                  </tr>
                                </thead>
                                @foreach($issued_requests as $issued_request)
                                <tbody>
                                  <tr>
                                    <th scope="row">{{ $issued_request->asset->name }} &nbsp;&nbsp; ( {{ $issued_request->asset->serial_number }} )</th>
                                    <td>{{ $issued_request->date_approved }}</td>
                                    <td>{{ $issued_request->date_returned }}</td>
                                  </tr>
                                </tbody>
                                @endforeach
                              </table>

                            </div>
                          </div>
                          <div class="x_panel">
                            <div class="x_title"  style="font-weight:bold;">Assets Declined</div>
                            <div class="x_content">
                              <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Asset</th>
                                    <th>Date Declined</th>
                                  </tr>
                                </thead>
                                @foreach($declined_requests as $declined_request)
                                <tbody>
                                  <tr>
                                    <th scope="row">{{ $declined_request->asset->name }} &nbsp;&nbsp; ( {{ $declined_request->asset->serial_number }} )</th>
                                    <td>{{ $declined_request->declined_date }}</td>
                                  </tr>
                                </tbody>
                                @endforeach
                              </table>

                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
              </div>

            </div>
          </div>


    </div>




</div>

@endsection
