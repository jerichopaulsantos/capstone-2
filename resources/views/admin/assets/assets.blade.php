@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

  <div class="container body">
      <div class="main-container">
          <div class="col-md-3 left_col"style="height:1750px;">
              <div class="left_col scroll-view">
                  <div class="clearfix"></div>
                  <br />
                  <!-- sidebar menu -->
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                      <h3></h3>
					<ul class="nav side-menu side-panel-font">
					  <li><a href="/home"><i class="fa fa-home mr10"></i> Dashboard</a>
					  </li>
					  <li><a href="#"><i class="fa fa-desktop mr10"></i> Assets</a>
					  </li>
					  <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
					  </li>
					  <li><a href="/requests/requests"><i class="fa fa-edit mr10"></i> Requests</a>
					  </li>
					</ul>
					</div>
					<div class="menu_section">
					<h3></h3>
					<ul class="nav side-menu side-panel-font">
					  <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
					  </li>
            <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                </a>
            </li>                 
					</ul>
                  </div>
                </div>
                <!-- /sidebar menu -->
            </div>
          </div> 

          <div class="top_nav">
          </div>



          <div class="row justify-content-center" style="margin-right: 0px!important;; margin-left: 0!important;">
            <div class="col-md-10 mt-5 mb-5">
                <div class="col-md-9">
                  <h1>Assets</h1>
                </div>
                <div class="col-md-3 mt-3">
		        <a href="/assets/create-asset" class="btn btn-primary btpr" style="float:right; width:100%;">Add Asset</a>
                </div>
            </div>
          </div>

          @foreach($assets as $asset)
          <div class="row justify-content-center" style="margin-right: 0px!important; margin-left: 0!important;">
            <div class="top_tiles col-md-10">
              <div class="animated flipInY col" style="padding-left: 5px;padding-right: 5px;">
                <div class="card mb-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card-img-bottom gradsh">
                        <img src="/{{ $asset->img_path }}" height="310px">
                      </div>
                    </div>
                    <div class="col-md-6 pb-3">
                      <div class="card-block" style="padding:25px;">
                        <h4 class="card-title">{{ $asset->name }}</h4>
                        <hr><br>
                        <div style="float:right; margin-top: -85px;">
                          <a href="{{route('assets.edit', ['id' => $asset->id])}}"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                          <a style="color: #73879c;" data-toggle="modal" data-target="#exampleModal2" onclick="setApproveReturnId({{ $asset->id }})" href="#"><i class="fa fa-close"></i></a>
                          <!-- <a href="{{route('assets.delete', ['id' => $asset->id])}}"></a> -->
                        </div>
                        <p class="card-text" style="margin-top:-20px;">
                          Type: {{ $asset->categoryname }} <br>
                          Processor: {{ $asset->processor }} <br>
                          Storage: {{ $asset->storage }} <br>
                          Memory: {{ $asset->memory }} <br>
                          Accessories: {{ $asset->accessories }} <br>
                          Serial Number: {{ $asset->serial_number }} <br>
                          <span style="color: #ec615b;">Issued to: {{ $asset->username !=null ? $asset->username : 'Asset available' }}</span> <br>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

          <div class="row justify-content-center" style="margin: 50px 0!important;">


            {{ $assets->links() }}

          </div>

      </div>
  </div>



<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Asset delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="get" action="{{route('assets.delete', ['id' => $asset->id])}}" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          <p>Are you sure to delete this asset?</p>
          <input type="hidden" id="approveReturnId" name="approveReturnId">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btda" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary btpr">Confirm</button>
        </div>
      </form>
    </div>
  </div>
</div>



@endsection