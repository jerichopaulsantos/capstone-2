@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

<div class="container body">

    <div class="main-container">
        <div class="col-md-3 left_col"style="height:1500px;">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/home"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/assets/assets"><i class="fa fa-desktop mr10"></i> Assets</a>
                      </li>
                      <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
                      </li>
                      <li><a href="/requests/requests"><i class="fa fa-edit mr10"></i> Requests</a>
                      </li>
                    </ul>
                    </div>
                    <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                 
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 

        <div class="top_nav">
        </div>

        <div class="row justify-content-center">
          <div class="col-md-10 mt-5">
              <div class="col-md-10">
                <p><a href="/assets/assets"><i class="fa fa-angle-left no-mobile"></i> &nbsp; Back to Assets</a></p>
              </div>
              <div class="col-md-10">
                <h1>Edit Asset</h1>
              </div>
              <div class="col-md-2 mt-3">
              </div>
          </div>
        </div>


          <div class="row justify-content-center mt-5 mb-5">
            <div class="top_tiles col-md-10">

              <div class="col offset-lg-3 col-lg-6 col-md-6 widget_tally_box justify-content-center ">
                <div class="x_panel ui-ribbon-container rounded shadow grads">
                  <div class="x_content">
                    <div class="flex">
                      <img src="/{{ $asset->img_path }}" alt="..." class="img-circle profile_img" style="border: none!important;margin:0 0 0 5px;background:none;width:100%;">
                    </div>
                    <div class="x_content mt-5">
                      <!-- start form for validation -->
                      <form method="post" action="{{route('assets.update', ['id' => $asset->id])}}" enctype="multipart/form-data">
                        @csrf
                        @if(count($errors) > 0)
                        <ul>
                          @foreach($errors->all() as $error)
                          <li style="color:#dc3545;">
                            {{ $error }}
                          </li>
                          @endforeach
                        </ul>
                        @endif
                        <label>Upload Image</label>
                        <input type="file" class="form-control" name="img_path" style="border: none; " />
                        <br>
                        <label>Asset Type</label>
                        <select class="form-control" name="category_id" required>
                          @foreach($categories as $category)
                          <option value="{{ $category->id }}" {{ $asset->category_id==$category->id ? 'selected': '' }}>{{ $category->name }}</option>
                          @endforeach
                        </select>
                        <br>
                        <label>Device Model</label>
                        <input type="text" class="form-control" name="name" value="{{ $asset->name }}" style="border: none; border-bottom: 1px solid #73879c;"required />
                        <br>
                        <label>Processor</label>
                        <input type="text" class="form-control" name="processor" value="{{ $asset->processor }}" style="border: none; border-bottom: 1px solid #73879c;" required />
                        <br>
                        <label>Storage</label>
                        <input type="text" class="form-control" name="storage" value="{{ $asset->storage }}" style="border: none; border-bottom: 1px solid #73879c;"required />
                        <br>
                        <label>Memory</label>
                        <input type="text" class="form-control" name="memory" value="{{ $asset->memory }}" style="border: none; border-bottom: 1px solid #73879c;" required />
                        <br>
                        <label>Serial Number</label>
                        <input type="text" class="form-control" name="serial_number" value="{{ $asset->serial_number }}" style="border: none; border-bottom: 1px solid #73879c;" required />
                        <br>
                        <label>Accessories</label>
                        <input type="text" class="form-control" name="accessories" value="{{ $asset->accessories }}" style="border: none; border-bottom: 1px solid #73879c;" required />
  	                    <br><small>All fields are required.</small><br><br>
	                    <button type="submit" class="btn btn-success btsu">Submit</button>
	                    <br><br>
                      </form>
                      <!-- end form for validations -->
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>


    </div>





</div>

@endsection