@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

<div class="container body">

    <div class="main-container">
        <div class="col-md-3 left_col"style="height:800px">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/home"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/assets/assets"><i class="fa fa-desktop mr10"></i> Assets</a>
                      </li>
                      <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
                      </li>
                      <li><a href="#"><i class="fa fa-edit mr10"></i> Requests</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 

        <div class="top_nav">
        </div>

        <div class="row justify-content-center">
          <div class="col-md-10 mt-5">
              <div class="col-md-10">
                <h1>Requests</h1>
              </div>
              <div class="col-md-2 mt-3">
              </div>
          </div>
        </div>


          <div class="row justify-content-center mt-5">
            <div class="top_tiles col-md-10">
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 " style="padding-left: 5px;padding-right: 5px;">
                <div class="tile-stats shadow">
                  <div class="count">{{ $laptop_count }}</div>
                  <h3>Laptops</h3>
                  <p>Available in stock</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 " style="padding-left: 5px;padding-right: 5px;">
                <div class="tile-stats shadow">
                  <div class="count">{{ $tablet_count }}</div>
                  <h3>Tablets</h3>
                  <p>Available in stock</p>
                </div>
              </div>
            </div>
          </div>


          <div class="row justify-content-center mt-5 mb-5">
            <div class="top_tiles col-md-10">
              @foreach($requests as $request)
              <div class="col-lg-4 col-md-4 col-sm-6 widget_tally_box">
                <div class="x_panel ui-ribbon-container rounded shadow">
                  <div class="x_content">
                    <div class="flex">
                      <img src="/{{ $request->user->img_path }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="name_title pt-3" style="font-size:16px;font-weight: bold;"><a href="{{route('users.view', ['id' => $request->user->id])}}">{{ $request->user->name }}</a></div>
                    <p style="margin-top: -10px;">{{ $request->user->position }}</p>
                    <div class="divider"></div>
                    <p class="ml-2">{{ $request->asset->category->name }} {{$request->type}}</p>

                    <div class="btn-group">
                      <button type="button" class="ml-3 mb-2 btn btn-danger btda dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" style="margin-top:-10px; margin-bottom:10px;">
                        Pending
                      </button>
                      <div class="dropdown-menu">
                        @if($request->type=='Request')
                        <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal1" onclick="setApproveRequestId({{ $request->id }})" href="#">Approved</a>
                        <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal3" onclick="setDeclineRequestId({{ $request->id }}, '{{ $request->user->email }}')" href="#">Decline</a>
                        @else
                        <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal2" onclick="setApproveReturnId({{ $request->id }})" href="#">Approved</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item"  href="#">Pending</a>
                      </div>
                    </div>
                    <div class="divider"></div>
                    <p>Priority: {{ $request->priority }}<br>
                    @if($request->type=='Request')
                    Message: <br> {{ $request->message }} </p>
                    @endif
                  </div>
                </div>
              </div>
              @endforeach

    </div>




</div>




<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Approve Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="/requests/approve-request" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          <p>Are you sure to approve this request?</p>
          <input type="hidden" id="approveRequestId" name="approveRequestId">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btda" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary btpr">Confirm</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Approve Return</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="/requests/approve-return" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          <p>Are you sure to approve this return?</p>
          <input type="hidden" id="approveReturnId" name="approveReturnId">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btda" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary btpr">Confirm</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm Decline Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="/requests/decline-request" enctype="multipart/form-data">
      @csrf
        <div class="modal-body">
          <p>Are you sure to decline this request?</p>
          <input type="hidden" id="declineRequestId" name="declineRequestId">
          <p>
            <strong>User email:</strong> &nbsp; <span id="declinedUserEmail"> </span> <br>
            <strong>Admin email:</strong> &nbsp; {{ Auth::user()->email }} <br>
            <strong>Subject:</strong> &nbsp; ASSET REQUEST - Declined <br>
          </p>
          <textarea name="message" class="form-control"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btda" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary btpr">Confirm</button>
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">

  function setApproveRequestId(requestId){
    $('#approveRequestId').val(requestId);
  }

  function setApproveReturnId(requestId){
    $('#approveReturnId').val(requestId);
  }

  function setDeclineRequestId(requestId, userEmail){
    $('#declineRequestId').val(requestId);
    $('#declinedUserEmail').html(userEmail);
  }



</script>



@endsection