@extends('layouts.app')

@section('content')

<div class="container body">

    <div class="main-container">
        <div class="col-md-3 left_col"style="height:100%;">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/assets/assets"><i class="fa fa-desktop mr10"></i> Assets</a>
                      </li>
                      <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
                      </li>
                      <li><a href="#"><i class="fa fa-edit mr10"></i> Requests</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 

        <div class="top_nav">
        </div>

        <div class="row justify-content-center">
          <div class="col-md-10 mt-5">
              <div class="col-md-10">
                <h1>Requests</h1>
              </div>
              <div class="col-md-2 mt-3">
              </div>
          </div>
        </div>


          <div class="row justify-content-center mt-5">
            <div class="top_tiles col-md-10">
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 " style="padding-left: 5px;padding-right: 5px;">
                <div class="tile-stats shadow">
                  <div class="count">7</div>
                  <h3>Laptops</h3>
                  <p>Available in stock</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 " style="padding-left: 5px;padding-right: 5px;">
                <div class="tile-stats shadow">
                  <div class="count">3</div>
                  <h3>Tablets</h3>
                  <p>Available in stock</p>
                </div>
              </div>
            </div>
          </div>


          <div class="row justify-content-center mt-5 mb-5">
            <div class="top_tiles col-md-10">

              <div class="col-lg-4 col-md-4 col-sm-6 widget_tally_box">
                <div class="x_panel ui-ribbon-container rounded shadow">
                  <div class="x_content">
                    <div class="flex">
                      <img src="{{ asset('images/user.png') }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="name_title pt-3" style="font-size:16px;font-weight: bold;"><a href="/users/view-user">Karmela Melawan</a></div>
                    <p style="margin-top: -10px;">Property Agent</p>
                    <div class="divider"></div>
                    <p class="ml-2" style="float:left; text-align:right;">Device<br>Request</p>
                    <div class="btn-group" style="float:right">
                      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Pending
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Approved</a>
                        <a class="dropdown-item" href="#">Decline</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Pending</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-6 widget_tally_box">
                <div class="x_panel ui-ribbon-container rounded shadow">
                  <div class="x_content">
                    <div class="flex">
                      <img src="{{ asset('images/user.png') }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="name_title pt-3" style="font-size:16px;font-weight: bold;"><a href="/users/view-user">Pedro Gil Puyat</a></div>
                    <p style="margin-top: -10px;">Property Agent</p>
                    <div class="divider"></div>
                    <p class="ml-2" style="float:left; text-align:right;">Device<br>Return</p>
                    <div class="btn-group" style="float:right">
                    <div class="btn-group" style="float:right">
                      <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Pending
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Approved</a>
                        <a class="dropdown-item" href="#">Decline</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Pending</a>
                      </div>
                    </div>
                    </div>                  
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-6 widget_tally_box">
                <div class="x_panel ui-ribbon-container rounded shadow">
                  <div class="x_content">
                    <div class="flex">
                      <img src="{{ asset('images/user.png') }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="name_title pt-3" style="font-size:16px;font-weight: bold;"><a href="/users/view-user">Sharon Corona</a></div>
                    <p style="margin-top: -10px;">Property Manager</p>
                    <div class="divider"></div>
                    <p class="ml-2" style="float:left; text-align:right;">Device<br>Request</p>
                    <div class="btn-group" style="float:right">
                      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Pending
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Approved</a>
                        <a class="dropdown-item" href="#">Decline</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Pending</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

    </div>




</div>

@endsection