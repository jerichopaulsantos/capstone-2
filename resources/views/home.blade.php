@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:30px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

<div class="container body">

    <div class="main-container">
        <div class="col-md-3 left_col"style="height:550px">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="#"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/assets/assets"><i class="fa fa-desktop mr10"></i> Assets</a>
                      </li>
                      <li><a href="/users/users"><i class="fa fa-user mr10"></i> Users</span></a>
                      </li>
                      <li><a href="/requests/requests"><i class="fa fa-edit mr10"></i> Requests</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 

        <div class="row justify-content-center">
          <div class="col-md-10">
              <div class="card mt-5 ad2"  style="height:200px">
                  <div class="card-body">
                    <div style="float:left">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                      <div style="font-size:30px; margin-top:30px;margin-left:20px">Hello {{ Auth::user()->name }}</div>
                      <p style="margin-left:20px">Welcome to the <span style="color:#ec6b65;">SkyBrokers App Manager</span> dashboard</p>
                    </div>
                  </div>
              </div>
          </div>
        </div>


          <div class="row justify-content-center mt-5">
            <div class="top_tiles col-md-10">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 " style="padding-left: 0px;padding-right: 10px;">
                <div class="tile-stats">
                  <div class="count">{{ $laptop_count }}</div>
                  <h3><a href="/assets/assets">Laptops</a></h3>
                  <p>Available in stock</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 " style="padding-left: 5px;padding-right: 10px;">
                <div class="tile-stats">
                  <div class="count">{{ $tablet_count }}</div>
                  <h3><a href="/assets/assets">Tablets</a></h3>
                  <p>Available in stock</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 " style="padding-left: 5px;padding-right: 10px;">
                <div class="tile-stats">
                  <div class="count">{{ $user_count }}</div>
                  <h3><a href="/users/users">Users</a></h3>
                  <p>Employees Registered</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 " style="padding-left: 5px;padding-right: 0px;">
                <div class="tile-stats">
                  <div class="count">{{ $allRequestCount }}</div>
                  <h3><a href="/requests/requests">Pending</a></h3>
                  <p>Requests and Returns</p>
                </div>
              </div>
            </div>
          </div>

    </div>


</div>
@endsection
