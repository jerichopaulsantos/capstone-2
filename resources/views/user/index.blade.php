@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

<div class="container body">


    <div class="main-container">
        <div class="col-md-3 left_col"style="height:600px;">
            <div class="left_col scroll-view">
                <div class="clearfix"></div>
                <br />
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="#"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/user/asset"><i class="fa fa-desktop mr10"></i> Asset Issued</a>
                      </li>
                      <li><a href="/user/request"><i class="fa fa-desktop mr10"></i> Request Asset</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div> 

        <div class="top_nav">
        </div>

        <div class="row justify-content-center">
          <div class="col-md-10">
              <div class="card mt-5 ad">
                  <div class="card-body">
                    <div style="float:left">
                      <!-- @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                      <div style="margin-top:30px;font-size:30px;">Hello {{ Auth::user()->name }}</div>
                      <p>Welcome to the SkyBrokers App Manager dashboard</p> -->
                    </div>
                  </div>
              </div>
          </div>
        </div>

        <div class="row justify-content-center">
            <div class="top_tiles col-md-10 mt-4">
              <div class="animated flipInY col-lg-8 col-md-8 col-sm-12 " style="padding-left: 0px;padding-right: 5px;">
                <div class="tile-stats pt-4" style="height:225px;">
                  <div class="count pl-3" style="; font-size:30px; !important;">Hello {{ Auth::user()->name }}</div>
                  <p class="pl-3">Asset issued to you by the company</p>
                  <hr class="mt-4 mb-4">
                  <div class="pb-4" style="margin:10px;">
                    @if($request!=null)
                    <span style="margin-left:20px;font-size: 16px;">{{ $request->asset->name }}</span>
                      @if($request->status_request=='pending')
                      <a href="#" class="btn btn-primary btpr disabled" style="float:right;font-size: 14px;">Pending Request</a>
                      @elseif($request->status_return=='pending')
                      <a href="/user/asset" class="btn btn-primary btpr" style="float:right;font-size: 14px;">Pending Return</a>
                      @elseif($request->status_request=='declined')
                      <a href="#" class="btn btn-danger btda disabled" style="float:right;font-size: 14px;">Declined</a>                      
                      @else
                      <a href="/user/asset" class="btn btn-primary btpr" style="float:right;font-size: 14px;">View</a>
                      @endif
                    @endif
                  </div>

                </div>
              </div>
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-12 " style="padding-left: 5px;padding-right: 0px;">
                <div class="tile-stats pt-4 pb-4" style="height:225px;">
                  <div class="count" style="font-size:30px; !important;">Request</div>
                  @if($request!=null)
                    @if($request->status_request=='pending')
                    <p>You have a pending asset request</p>
                    <hr class="mt-4 mb-4">
                    <a href="/user/request" class="btn btn-primary btpr disabled" style="margin:10px;float:left;font-size: 14px;">Request Asset</a>
                    @else
                    <p>You currently have an asset</p>
                    <hr class="mt-4 mb-4">
                    <a href="/user/request" class="btn btn-primary btpr disabled" style="margin:10px;float:left;font-size: 14px;">Request Asset</a>
                    @endif
                  @else
                    @if($assetCount==0)
                    <p>There are no available assets</p>
                    <hr class="mt-4 mb-4">
                    <a href="/user/request" class="btn btn-primary btpr disabled" style="margin:10px;float:left;font-size: 14px;">Request Asset</a>
                    @else
                    <p>Request a company-issued asset</p>
                    <hr class="mt-4 mb-4">
                    <a href="/user/request" class="btn btn-primary btpr" style="margin:10px;float:left;font-size: 14px;">Request Asset</a>
                    @endif
                  @endif
                </div>
              </div>
            </div>
        </div>




    </div>




</div>
@endsection
