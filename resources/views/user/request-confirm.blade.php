@extends('layouts.app')

@section('content')
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    <img src="{{ asset('images/skybrokers-app.png') }}" style="height:40px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <img src="/{{ Auth::user()->img_path }}" alt="..." class="img-circle profile_img" style="width:35px;margin-right:10px;margin-top:10px;">
                            </li> 
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>


                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="left: -70px;">
                                    @if(Auth::user()->is_owner)
                                    <a class="dropdown-item"  href="/users/admin">Other Admin</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

  <div class="container body">
      <div class="main-container">
          <div class="col-md-3 left_col"style="height:720px;">
              <div class="left_col scroll-view">
                  <div class="clearfix"></div>
                  <br />
                  <!-- sidebar menu -->
                  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                      <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/user/"><i class="fa fa-home mr10"></i> Dashboard</a>
                      </li>
                      <li><a href="/user/asset"><i class="fa fa-desktop mr10"></i> Asset Issued</a>
                      </li>
                      <li><a href="/user/request"><i class="fa fa-desktop mr10"></i> Request Asset</a>
                      </li>
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3></h3>
                    <ul class="nav side-menu side-panel-font">
                      <li><a href="/users/view-user/{{ Auth::user()->id }}"><i class="fa fa-cog mr10"></i> Profile</a>
                      </li>
                      <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr10"></i> {{ __('Logout') }}
                          </a>
                      </li>                
                    </ul>
                    </div>
                  </div>
                  <!-- /sidebar menu -->
              </div>
          </div> 

          <div class="top_nav">
          </div>

          <div class="row justify-content-center" style="margin-right: 0px!important; margin-left: 0!important;">
            <div class="col-md-10 mt-5 mb-5">
                <div class="col-md-10">
                  <h1 class="text-align">Request Asset</h1>
                </div>
                <div class="col-md-2 mt-3">
                </div>
            </div>
          </div>

          <div class="row justify-content-center" style="margin-right: 0px!important; margin-left: 0!important;">
            <p style="color: #b9c7d5;">Success! We have received your asset request. Expect a decision within the next 24 hours.</p>
            <img class="col-lg-7" src="{{ asset('images/success.png') }}">
          </div>

      </div>
  </div>

@endsection
